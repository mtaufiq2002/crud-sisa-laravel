@extends('layouts.master')

@section('content')
    <div class="main">
        <div class="main-content">
            <div class="container-fluid">
                <div class="row">
                    <div clasa="col-md-12">
                    <div class="panel">
                      <div class="panel-heading">
                        <h3 class="panel-title">Data Siswa</h3>
                        <div class="right">
                        <button type="button" class="btn"><i class="lnr lnr-plus-circle" data-toggle="modal" data-target="#exampleModal"></i></button>
                          </button>
                          
                        </div>
                      </div>
                      <div class="panel-body">
                        <table class="table table-hover">
                          <thead>
                          <tr>
                            <th>NAMA</th>
                            <th>KELAS</th>
                            <th>ALAMAT</th>
                            <th>TEMPAT LAHIR</th>
                            <th>TANGGAL LAHIR</th>
                            <th>AKSI</th>
                            <th>AKSI</th>
                        </tr>
                          </thead>
                          <tbody>
                          @foreach($data_siswa as $siswa)
                        <tr>
                            <td><a href="/siswa/{{$siswa->id}}/profile">{{$siswa->nama}}</a></td>
                            <td>{{$siswa->kelas}}</td>
                            <td>{{$siswa->alamat}}</td>
                            <td>{{$siswa->tempat_lahir}}</td>
                            <td>{{$siswa->tanggal_lahir}}</td>   
                            <td><a href="/siswa/{{$siswa->id}}/edit" class="btn btn-sm btn-warning" >Edit</a></td> 
                            <td><a href="/siswa/{{$siswa->id}}/delete" class="btn btn-sm btn-danger" onclick="return confirm('Yakin Mau Di Hapus....?')" >Delete</a></td>
                        </tr>
                        @endforeach
                        </table>
                      </div>
                    </div>
                    </div>

                </div>
              
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Data Siswa</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                      <form action="/siswa/create" method="POST">
                        <div class="form-group">
                        {{csrf_field()}}
                          <label for="exampleInputEmail1">NAMA</label>
                          <input name="nama" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                          <small id="emailHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">KELAS</label>
                          <input name="kelas" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                          <small id="emailHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">ALAMAT</label>
                          <input name="alamat" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                          <small id="emailHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">TEMPAT LAHIR</label>
                          <input name="tempat_lahir" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                          <small id="emailHelp" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">TANGGAL LAHIR</label>
                          <input name="tanggal_lahir" type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                          <small id="emailHelp" class="form-text text-muted"></small>
                        </div>
                      
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

@stop

    