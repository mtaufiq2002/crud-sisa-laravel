<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';
    protected $fillable =['nama','kelas','alamat','tempat_lahir','tanggal_lahir','avatar'];

} 
