<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiswaController extends Controller
{
    public function index()
    {
        $data_siswa = \App\Siswa::all();
        return view('siswa.index',['data_siswa' =>$data_siswa]);
    }

    public function create(Request $request)
    {
        \App\siswa::create($request->all());
        return redirect('/siswa')->with('sukses','Data Berhasil Di Simpan');
    }

    public function edit($id)
    {
        $siswa = \App\siswa::find($id);
        return view('siswa/edit',['siswa'=>$siswa]);
    }
    public function update(Request $request,$id)
    {
        $siswa = \App\siswa::find($id);
        $siswa->update($request->all());
        return redirect('/siswa')->with('sukses','Data Berhasil DI Update');
    }
    public function delete($id)
    {
        $siswa = \App\siswa::find($id);
        $siswa->delete($siswa);
        return redirect('/siswa')->with('Data Selesai Di Hapus');
    }
    public function profile($id)
    {
        $siswa = \App\siswa::find($id);
        return view('siswa.profile',['siswa' => $siswa]);
    }
    
}